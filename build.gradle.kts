import org.jetbrains.kotlin.gradle.tasks.KotlinCompile



plugins {
    kotlin("jvm") version "1.7.10"
    kotlin("plugin.serialization") version "1.6.10"
    id("org.owasp.dependencycheck") version "6.5.3"
    id("com.github.jk1.dependency-license-report") version "2.0"
    application
    `maven-publish`
    java
}

group = "id.walt"
version = "1.2"

repositories {
    mavenLocal()
    mavenCentral()
    maven("https://jitpack.io")
    maven("https://maven.walt.id/repository/waltid/")
    maven("https://maven.walt.id/repository/waltid-ssi-kit/")
    maven("https://repo.danubetech.com/repository/maven-public/")
    maven("https://gitlab.com/api/v4/projects/46463849/packages/maven")
    maven("https://gitlab.com/api/v4/projects/52159053/packages/maven")
}

dependencies {
    // SSIKIT
    //implementation("id.walt:waltid-ssikit:1.9")
    implementation("id.walt:forked-waltid-ssikit:1.7")
    //implementation("id.walt:waltid-ssikit-vclib:1.24.2")

    // Metaco
    // implementation("com.metaco:sdk:2.1.0")
    implementation ("org.web3j:core:4.10.0")
    implementation("io.javalin:javalin-bundle:4.6.4")
    implementation("com.github.kmehrunes:javalin-jwt:0.3")
    implementation("com.beust:klaxon:5.6")
    implementation("com.nimbusds:oauth2-oidc-sdk:9.43.1")
    implementation("com.github.multiformats:java-multibase:v1.0.0")
    // CLI
    implementation("com.github.ajalt.clikt:clikt-jvm:3.5.1")
    implementation("com.github.ajalt.clikt:clikt:3.5.1")

    // Service-Matrix
    implementation("id.walt.servicematrix:WaltID-ServiceMatrix:1.1.3")

    // Logging
    //implementation("org.slf4j:slf4j-api:2.0.5")
    implementation("org.slf4j:slf4j-simple:2.0.5")
    implementation("io.github.microutils:kotlin-logging-jvm:3.0.4")
    implementation ("io.github.cdimascio:dotenv-kotlin:6.4.1")

    // Ktor
    implementation("io.ktor:ktor-client-jackson:2.2.4")
    implementation("io.ktor:ktor-client-content-negotiation:2.2.4")
    implementation("io.ktor:ktor-serialization-kotlinx-json:2.2.4")
    implementation("io.ktor:ktor-client-core:2.2.4")
    implementation("io.ktor:ktor-client-cio:2.2.4")
    implementation("io.ktor:ktor-client-logging:2.2.4")
    implementation("io.ktor:ktor-client-auth:2.2.4")

    // Cache
    implementation("io.github.pavleprica:kotlin-cache:1.2.0")

    // Testing
    //testImplementation(kotlin("test-junit"))
    testImplementation("io.mockk:mockk:1.13.2")

    testImplementation("io.kotest:kotest-runner-junit5:5.5.4")
    testImplementation("io.kotest:kotest-assertions-core:5.5.4")
    testImplementation("io.kotest:kotest-assertions-json:5.5.4")

    // HTTP
    implementation("io.ktor:ktor-client-core:2.2.4")
    implementation("io.ktor:ktor-client-content-negotiation:2.2.4")
    implementation("io.ktor:ktor-client-cio:2.2.4")
    implementation("io.ktor:ktor-client-logging:2.2.4")
    implementation(kotlin("stdlib-jdk8"))
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "16"
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

application {
    mainClass.set("id.walt.MainKt")
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            artifactId = "forked-waltid-walletkit"
            pom {
                name.set("walt.id wallet Kit")
                description.set(
                    """
                    Kotlin/Java library for SSI core services, with primary focus on European EBSI/ESSIF ecosystem.
                    """.trimIndent()
                )
            }
            from(components["java"])
        }
    }

    repositories {
          maven {
        url = uri("https://gitlab.com/api/v4/projects/44687731/packages/maven")
        credentials(HttpHeaderCredentials::class) {
            name = "Job-Token"
            value = System.getenv("CI_JOB_TOKEN")
        }
        authentication {
            create("header", HttpHeaderAuthentication::class)
        }
    }
    }
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "16"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "16"
}